package com.appWarung;
import java.util.Scanner;

class Warung {

  public static void main(String[] args) {
    Menu menu = new Menu();
    Pesan pesan = new Pesan();
    Scanner input = new Scanner(System.in);

    System.out.println("Selamat Datang di Warung Kami");
    for(;;) {
      System.out.println("Silahkan pilih makanan kesukaan anda");
      System.out.println("=======================================");

      menu.getMakanan();

      System.out.println("Pilih makanan anda : ");
      int kodemakanan = input.nextInt();
      System.out.println("Mau Pesan berapa? ");
      int jumlah = input.nextInt();
      pesan.pesanMakanan(kodemakanan, jumlah);

      System.out.println("Apakah anda ingin memesan lagi? (Y/T) : ");
      String tanya = input.next();
      if( tanya.equals("Y") || tanya.equals("y") ) {
        pesan.count++;
        pesan.i++;
      } else if( tanya.equals("T") || tanya.equals("t") ) {
        pesan.hitungPesanan();
        System.out.println("Total harga yang harus dibayar : " + pesan.getAll());
        System.out.println("Masukkan jumlah uang yang akan dibayar : ");
        int bayaran = input.nextInt();
        if( bayaran > pesan.all ) {
          System.out.println("Kembalian anda adalah : " + pesan.kembalian(bayaran));
          System.out.println("Terimakasih telah membeli :D");
        } else if(bayaran < pesan.all) {
          System.out.println("Maaf uang anda tidak cukup :(");
        } else if(bayaran == pesan.all) {
          System.out.println("Terimakasih telah membeli :D");
        }
        System.exit(0);
      }

    }

  }

}
