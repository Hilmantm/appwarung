package com.appWarung;
import java.util.Scanner;

class Pesan {

  Menu menu = new Menu();
  int[][] pesanan = new int[9999][3];
  int i = 0, j = 0, count = 0, jumlah = 0, all = 0;

  public void pesanMakanan(int kodemakanan, int jumlah) {
    switch(kodemakanan) {
      case 1:
        pesanan[i][j] = 0;
        break;
      case 2 :
        pesanan[i][j] = 1;
        break;
      case 3 :
        pesanan[i][j] = 2;
        break;
      case 4 :
        pesanan[i][j] = 3;
        break;
      default :
        System.out.println("Pilihan tidak ada");
    }

    pesanan[i][1] = jumlah;

  }

  public void hitungPesanan() {
    System.out.println("Pesanan anda adalah : ");
    System.out.println("No\tmakanan\tHarga\tJumlah\tTotal");
    for(int k = 0; k <= count; k++) {
      int total = menu.harga[pesanan[k][0]]*pesanan[k][1];
      all += total;
      System.out.println((k+1) + ".) " + "\t" + menu.makanan[pesanan[k][0]] + "\t" + menu.harga[pesanan[k][0]] + "\t" + pesanan[k][1] + "\t" + total);
    }
  }

  public int getAll() {
    return all;
  }

  public int kembalian(int bayaran) {
    int kembalianPelanggan = bayaran - all;
    return kembalianPelanggan;
  }

}
